* Se incluyen unos cuantos jars que se corresponden con diversos stubs de WS ya existentes, que deben instalarse en el repo local de Maven para poder realizar la compilzaci�n inicial. Las coordenadas que tienen que llevar dichos artefactos se encuentran comentadas en el fichero pom.xml.

mvn install:install-file -Dfile=scontratacion-1.19.0.jar -DgroupId=sanitas.bravo.clientes -DartifactId=scontratacion -Dversion=1.19.0 -Dpackaging=jar

mvn install:install-file -Dfile=ws_contratacion-1.2.28.jar -DgroupId=sanitas.bravo.clientes -DartifactId=ws_contratacion -Dversion=1.2.28 -Dpackaging=jar

mvn install:install-file -Dfile=rest_simulacionpoliza-2.15.0.jar -DgroupId=sanitas.bravo.clientes -DartifactId=rest_simulacionpoliza -Dversion=2.15.0 -Dpackaging=jar

* El entregable debe compilar, en una instalación de maven totalmente limpia, debe indicarse en un fichero README los pasos necesarios para generar el jar final (no hace falta mucho detalle).

mvn clean package