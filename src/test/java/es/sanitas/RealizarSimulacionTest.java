package es.sanitas;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.ReciboProducto;
import es.sanitas.soporte.Recibo;

/**
 * Unit test for simple App.
 */
public class RealizarSimulacionTest{


	/**
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	@Test
	public void reciboTest() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {

		ReciboProducto rp = new ReciboProducto();
		RealizarSimulacion rs = RealizarSimulacion.class.newInstance();

		Method m = RealizarSimulacion.class.getDeclaredMethod("toRecibo",new Class[]{ReciboProducto.class});
		//m.invoke(d);// throws java.lang.IllegalAccessException
		m.setAccessible(true);// Abracadabra 
		
		rp.setIdProducto(1);
		rp.setDescripcion("test");
		rp.setRecibosProducto(null);
		Recibo reciboResult = (Recibo) m.invoke(rs,rp);// now its OK


		Recibo reciboParamTestResultExpected = new Recibo();
		final Calendar fechaEmision = Calendar.getInstance();
		try {
			fechaEmision.setTime(  new SimpleDateFormat( "dd/MM/yyyy" ).parse( "25/12/2016" ) );
		} catch( final ParseException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reciboParamTestResultExpected.setFechaEmision( fechaEmision );
		reciboParamTestResultExpected.setImporte( 1000. );
		
		
		assertEquals(reciboParamTestResultExpected, reciboResult);
	}

}
